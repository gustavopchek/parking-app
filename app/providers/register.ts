import {Injectable,Inject} from '@angular/core';
import {Http,Headers,Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {NavController, AlertController} from 'ionic-angular';
import {Customer} from '../models/customer';
import {TabsPage} from '../pages/tabs/tabs';
import {StorageUtils} from '../utils/storage.utils';
import {API_URL} from './config';

const CONTENT_TYPE_HEADER:string = 'Content-Type';
const APPLICATION_JSON:string = 'application/json';
// const BACKEND_URL:string = 'http://demo2726806.mockable.io/authenticate';
const BACKEND_URL:string = API_URL + '/api/v1/registrations';

@Injectable()
export class RegisterService {
    constructor(private http:Http, public alertCtrl: AlertController) {}
    
    register(name:string, phone:string, cpf:string, email:string, password:string, passwordConfirmation:string):Observable<any> {
        // if(username.toLowerCase() !== 'admin' || password.toLowerCase() !== 'admin') {
        //     let alert = this.alertCtrl.create({
        //         title: 'Invalid credentials',
        //         subTitle: 'You entered invalid credentials !',
        //         buttons: ['Ok']
        //     });
        //       alert.present();
        //     return Observable.throw(alert);
        // } else {

            let headers = new Headers();
            headers.append(CONTENT_TYPE_HEADER, APPLICATION_JSON);
            

            // email = "gustavohpk@yahoo.com";
            // password = "123456";

            // console.log(JSON.stringify({customer:{name: name, phone: phone, cpf: cpf, email: email,password: password, passwordConfirm: passwordConfirmation}}));

            return this.http.post(BACKEND_URL,JSON.stringify({user:{name: name, phone: phone, cpf: cpf, email: email,password: password, password_confirmation: passwordConfirmation}}),{headers:headers}).map((res:Response) => {

                let registerData:any = res.json();
                // let customer:Customer = this.readJwt(registerData.token);
                let customer:Customer = new Customer(registerData);

                console.log('Register successful', customer);

                // if (rememberMe) {
                    console.log('Remember me: Store user and jwt to local storage');
                    StorageUtils.setAccount(customer);
                    StorageUtils.setToken(registerData.token);
                // }

                return customer;
            });
        // }
    }
    // private readJwt(token:string):Customer {
    //     let tokens:Array<any> = token.split('.');
    //     let tokenPayload:any = JSON.parse(atob(tokens[1]));

    //     let customer:Customer = new Customer();
    //     customer.lastConnection = new Date();
    //     customer.id = parseInt(tokenPayload.iss);
    //     customer.email = tokenPayload.sub;
    //     customer.firstName = tokenPayload.firstName;
    //     customer.lastName = tokenPayload.lastName;
    //     customer.roles = tokenPayload.role;

    //     return customer;
    // }
}