import { Injectable } from '@angular/core';
import { Http, Headers,Response } from '@angular/http';
import 'rxjs/add/operator/map';
import {API_URL} from './config';
import {Customer} from '../models/customer';
import {StorageUtils} from '../utils/storage.utils';
// import {Observable} from 'rxjs/Observable';
// import { Subject } from 'rxjs/Subject';
import { Events } from 'ionic-angular';


/*
  Generated class for the Customer provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class CustomerService {

  headers: Headers;
  private customerObserver: any;
  public obs: any;

  constructor(private http: Http, public events: Events) {
    this.headers = new Headers();

    // this.customerObserver = null;

    // this.obs = Observable.create(observer => {
    //     this.customerObserver= observer;
    // });
    // this.obs = new Subject();
  }


  // public notify():void {
  //   this.customerObserver.next('testcde');
  // }

  getCustomer() {
    let data = StorageUtils.getAccount();
    // if(data.id){
     return this.http.get(API_URL + '/api/v1/customers/'+data.id, {headers: this.headers}).map((res:Response) => {

          let customerData:any = res.json();
          // console.log(loginData);
          // let customer:Customer = this.readJwt(loginData.token);
          let customer:Customer = new Customer(customerData);
          // customer.email = email;
          // customer.password = password;

          console.log('Get Customer Successful', customer);

          // if (rememberMe) {
              // console.log('Remember me: Store user and jwt to local storage');
              StorageUtils.setAccount(customer);
              
          // }
          // this.customerObserver.next(customer);
          this.events.publish('customer:updated', customer);

          return customer;
      });
    // }
  }

}

