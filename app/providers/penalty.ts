import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import {API_URL} from './config';

/*
  Generated class for the Penalty provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class PenaltyService {

  headers: Headers;

  constructor(private http: Http) {
    this.headers = new Headers();
  }

  getPenalties() {
    let penalties = this.http.get(API_URL + '/api/v1/penalties', {headers: this.headers});
    return penalties;
  }

  customerPenalties() {
    let penalties = this.http.get(API_URL + '/api/v1/penalties/customer', {headers: this.headers});
    return penalties;
  }

  platePenalties() {
    let penalties = this.http.get(API_URL + '/api/v1/penalties/plate/1', {headers: this.headers});
    return penalties;
  }

  payPenalty(params){
    var link = API_URL + '/api/v1/penalties/payment/';
    var headers = this.headers;
    headers.append('Content-Type', 'application/json');

    console.log(params);
    
    return this.http.post(link, {penalty_id: params}, {headers: headers})
    .map(res => res.json());
    // .catch(this.handleError());

    // return response;

  }

}

