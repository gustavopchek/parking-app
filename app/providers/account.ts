import {Injectable,Inject} from '@angular/core';
import {Http,Headers,Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {NavController, AlertController} from 'ionic-angular';
import {Customer} from '../models/customer';
import {TabsPage} from '../pages/tabs/tabs';
import {StorageUtils} from '../utils/storage.utils';
import {API_URL} from './config';

@Injectable()
export class AccountService {
  constructor(private http:Http, public alertCtrl: AlertController) {}
  
  account(name:string, phone:string, cpf:string, email:string, password:string, passwordConfirmation:string, currentPassword:string):Observable<any> {

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    let data = StorageUtils.getAccount();
    var customerData;

    if(password.length > 0){
      customerData = JSON.stringify({user:{id: data.id.toString(), name: name, phone: phone, cpf: cpf, email: email,password: password, password_confirmation: passwordConfirmation, current_password: currentPassword},type: "customer"});
    }
    else{
      customerData = JSON.stringify({user:{id: data.id.toString(), name: name, phone: phone, cpf: cpf, email: email, current_password: currentPassword},type: "customer"});
    }

    console.log(data.id);
    console.log(customerData);

    return this.http.patch(API_URL + '/api/v1/registrations/'+data.id,customerData,{headers:headers}).map((res:Response) => {

      let accountData:any = res.json();
      let customer:Customer = new Customer(accountData);

      StorageUtils.setAccount(customer);

      return customer;
    });
  }
}