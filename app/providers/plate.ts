import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import {API_URL} from './config';

/*
  Generated class for the Plate provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class Plate {

	headers: Headers;

  constructor(private http: Http) {
  	this.headers = new Headers();
  }

  getPlates() {
    let plates = this.http.get(API_URL + '/api/v1/plates', {headers: this.headers});
    return plates;
  }

  customerPlates() {
    let plates = this.http.get(API_URL + '/api/v1/plates/customer', {headers: this.headers});
    console.log(plates);
    return plates;
  }

  addPlate(params){
  	var link = API_URL + '/api/v1/plates/';
  	var headers = this.headers;
    headers.append('Content-Type', 'application/json');

    console.log(params);
    
    return this.http.post(link, params, {headers: headers})
    .map(res => res.json());
    // .catch(this.handleError());

    // return response;

  }

  removePlate(id){
    var link = API_URL + '/api/v1/plates/'+id;
    var headers = this.headers;
    console.log(link);
    headers.append('Content-Type', 'application/json');
    
    return this.http.delete(link, {headers: headers})
    .map(res => res.json());

  }

}

