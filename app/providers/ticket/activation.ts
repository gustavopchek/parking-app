import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import {API_URL} from './../config';

/*
  Generated class for the Activation provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class Activation {

  headers: Headers;

  constructor(private http: Http) {
    this.headers = new Headers();
  }

  getActivations() {
    let activations = this.http.get(API_URL + '/api/v1/activations', {headers: this.headers});
    return activations;
  }

  lastActivation() {
    let activation = this.http.get(API_URL + '/api/v1/activations/last_activation', {headers: this.headers});
    return activation;
  }

  customerActivations() {
    let activations = this.http.get(API_URL + '/api/v1/activations/customer', {headers: this.headers});
    return activations;
  }

  createActivation(params){
  	var link = API_URL + '/api/v1/activations/';
  	var headers = this.headers;
    // console.log(params);
    headers.append('Content-Type', 'application/json');
    
    return this.http.post(link, params, {headers: headers})
    .map(res => res.json());
    // .catch(this.handleError());

    // return response;

  }

}

