import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import {API_URL} from './../config';

/*
  Generated class for the Ticket provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class Ticket {

	headers: Headers;

  constructor(private http: Http) {
  	this.headers = new Headers();
  }

  getTickets() {
    let tickets = this.http.get(API_URL + '/api/v1/tickets', {headers: this.headers});
    return tickets;
  }

}

