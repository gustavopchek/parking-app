import {Injectable,Inject} from '@angular/core';
import {Http,Headers,Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {NavController, AlertController} from 'ionic-angular';
import {Customer} from '../models/customer';
import {TabsPage} from '../pages/tabs/tabs';
import {StorageUtils} from '../utils/storage.utils';
import {API_URL} from './config';

@Injectable()
export class LoginService {
  constructor(private http:Http, public alertCtrl: AlertController) {}
  
  login(email:string,password:string,rememberMe:boolean):Observable<any> {

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    
    return this.http.post(API_URL + '/api/v1/sessions',JSON.stringify({user:{email: email,password: password, type: "customer"}}),{headers:headers}).map((res:Response) => {

      let loginData:any = res.json();
      let customer:Customer = new Customer(loginData);

      StorageUtils.setAccount(customer);
      StorageUtils.setToken(loginData.token);

      return customer;
    });
  }
}