import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import {API_URL} from './config';

/*
  Generated class for the Purchase provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class Purchase {

  headers: Headers;

  constructor(private http: Http) {
    this.headers = new Headers();
  }

  getPurchases() {
    let purchases = this.http.get(API_URL + '/api/v1/purchases', {headers: this.headers});
    return purchases;
  }

  customerPurchases() {
    let purchases = this.http.get(API_URL + '/api/v1/purchases/customer', {headers: this.headers});
    console.log(purchases);
    return purchases;
  }

  createPurchase(params){
  	var link = API_URL + '/api/v1/purchases/';
  	var headers = this.headers;
    headers.append('Content-Type', 'application/json');

    console.log(params);
    
    return this.http.post(link, params, {headers: headers})
    .map(res => res.json());
    // .catch(this.handleError());

    // return response;

  }

}

