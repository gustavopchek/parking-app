import {Component, ViewChild, provide, NgZone } from '@angular/core';
import {Platform, ionicBootstrap, MenuController, NavController, Events} from 'ionic-angular';
import {Http,RequestOptions, XHRBackend} from '@angular/http';
import {StatusBar} from 'ionic-native';
import {LoginPage} from './pages/login/login';
import {TabsPage} from './pages/tabs/tabs';
import {HomePage} from './pages/home/home';
import {ActivationsPage} from './pages/activations/activations';
import {CashPage} from './pages/cash/cash';
import {AboutPage} from './pages/about/about';
import {ContactPage} from './pages/contact/contact';
import {AccountPage} from './pages/account/account';
import {StorageUtils} from './utils/storage.utils';
import {LoginService} from './providers/login';
import {AuthorizationHandler} from './utils/authorization-handler';
import {CustomerService} from './providers/customer';

@Component({
  templateUrl: 'build/app.html',
  providers: [NavController, LoginService, CustomerService,
    provide(Http,{
      useFactory:(xhrBackend: XHRBackend, requestOptions: RequestOptions) => {
        return new AuthorizationHandler(xhrBackend, requestOptions);
      },
      deps: [XHRBackend, RequestOptions],
      multi:false
    })
  ]
})
export class WayPark {
  @ViewChild('nav') nav : NavController;

  private rootPage: any;
  private pages: any[];
  customer: any;

  constructor(private platform: Platform, private menu: MenuController, private loginService: LoginService, private ngZone: NgZone, public events: Events) {
    
    this.events.subscribe('customer:updated', (customerData) => {
      this.customer = customerData[0];
    });

    this.menu = menu;
    this.pages = [
      { title: 'Comprar créditos', component: CashPage, icon: 'cash' },
      { title: 'Sobre', component: AboutPage, icon: 'information-circle' },
      { title: 'Contato', component: ContactPage, icon: 'contacts' },
      { title: 'Minha conta', component: AccountPage, icon: 'contact' }
    ];
    
    if (StorageUtils.hasAccount()) {
      this.rootPage = TabsPage;
    } else {
      this.rootPage = LoginPage;
    }

    platform.ready().then(() => {
      StatusBar.styleDefault();
    });
  }

  ngOnInit(){
    this.customer =  StorageUtils.getAccount();
  }

  openPage(page) {
    this.menu.close()
    this.nav.push(page.component);
  };

  logout():void {
    StorageUtils.removeToken();
    StorageUtils.removeAccount();
    this.menu.enable(false);
    this.nav.setRoot(LoginPage);
  }
}

ionicBootstrap(WayPark);
