import {Component} from '@angular/core';
import {NavController, Events, AlertController} from 'ionic-angular';
import {Purchase} from '../../providers/purchase';
import {CustomerService} from '../../providers/customer';
import {StorageUtils} from '../../utils/storage.utils';

@Component({
  templateUrl: 'build/pages/cash/cash.html',
  providers: [Purchase, CustomerService]
})
export class CashPage {

	purchaseParams = {value: 0};
	customer: any;
  errors = [];
	public loading;

  constructor(private nav: NavController, private p: Purchase, private customerService: CustomerService, public events: Events, private alertCtrl: AlertController) {}

  public purchase() {
  	// this.customer =  StorageUtils.getAccount();
  	// console.log(this.customer);
  	// if(this.customer){
  	// 	this.purchaseParams.customer_id = this.customer.id;
  	// }

    var result = false;

    if(this.purchaseParams.value != 10 && this.purchaseParams.value != 20 && this.purchaseParams.value != 30){
      this.errors = [];
      this.errors.push('Valor incorreto.');
    }else{
      this.errors = [];
      var confirm = this.alertCtrl.create({
        title : 'Comprar créditos',
        message : 'Confirmar compra de R$'+ this.purchaseParams.value +',00 em créditos?',
        buttons: [
          {text: 'Cancelar', role:'cancel', handler : () => {}},
          {text: 'Sim', handler : () => {
            
            this.loading = true;
            var params = JSON.stringify({purchase : 
              this.purchaseParams
            });
            this.p.createPurchase(params).subscribe(
              data => {
                console.log('purchased');
                result = true;
                this.customerService.getCustomer().subscribe();
                this.events.publish('purchases:updated', data);
                setTimeout(() => this.loading = false, 400);
                setTimeout(() => this.nav.popToRoot(), 500);
              },
              err => {
                this.loading = false;
                this.errors = err.json().errors;
                console.error(err)
              },
              () => null
            );

            confirm.dismiss().then(() => {
            })
          }}
        ]})
        
      confirm.present();
    }
    
  }
 
}
