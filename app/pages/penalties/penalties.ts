import {Component} from '@angular/core';
import {NavController, Events} from 'ionic-angular';
import {PenaltyService} from '../../providers/penalty';
import {PenaltyPage} from '../penalty/penalty';


@Component({
  templateUrl: 'build/pages/penalties/penalties.html',
  providers: [PenaltyService]
})
export class PenaltiesPage {

	public foundPenalties = [];

  constructor(private navCtrl: NavController, private penalties: PenaltyService, public events: Events) {
    this.events.subscribe('penalties:updated', (penaltiesData) => {
      this.getPenalties();
      // penaltiesData[0].created_at = new Date(penaltiesData[0].created_at).toLocaleString([], {day: '2-digit', month: '2-digit', year: '2-digit', hour: '2-digit', minute:'2-digit'});
      // this.foundPenalties.unshift(penaltiesData[0]);
      // console.log(penaltiesData[0]);
    });
  }
	ngOnInit() {
    this.getPenalties();
    // setInterval(() => this.getCustomerPenalties(), 60000);
  }

  getPenalties() {
    this.penalties.customerPenalties().subscribe(
      data => {
        this.foundPenalties = data.json();
        for (var key in this.foundPenalties) {
          this.foundPenalties[key].created_at = new Date(this.foundPenalties[key].created_at).toLocaleString([], {day: '2-digit', month: '2-digit', year: '2-digit', hour: '2-digit', minute:'2-digit'});
          this.foundPenalties[key].limit = new Date(this.foundPenalties[key].limit).toLocaleString([], {day: '2-digit', month: '2-digit', year: '2-digit', hour: '2-digit', minute:'2-digit'});
          // if(this.foundPenalties[key].penalty_payment){
          //   new Date(this.foundPenalties[key].penalty_payment.created_at).toLocaleString([], {day: '2-digit', month: '2-digit', year: '2-digit', hour: '2-digit', minute:'2-digit'});
          // }
        };
        console.log(this.foundPenalties)
      },
      err => console.error(err),
      () => console.log('getPenalties completed')
    );
	}

  openPenalty(penalty) {
    // console.log('penalty open');
    // penalty.created_at = new Date(penalty.created_at).toLocaleString()
    this.navCtrl.push(PenaltyPage, {
      penalty: penalty
    });
  };
}
