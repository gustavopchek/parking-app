import {Page, NavController, Alert} from 'ionic-angular';
import {Inject} from '@angular/core';
import {FormBuilder, Validators, FormGroup} from '@angular/forms';
import {TabsPage} from '../tabs/tabs';
import {RegisterPage} from '../register/register';
import {Customer,Profile} from '../../models/customer';
import {LoginService} from '../../providers/login';
import {Response} from '@angular/http';
import {StorageUtils} from '../../utils/storage.utils';

@Page({
    selector:'login-page',
    templateUrl: 'build/pages/login/login.html',
    providers:[LoginService]
})
export class LoginPage {
  loginForm:FormGroup;
  rememberMe = false;
  errors = [];
  public loading = false;
  constructor(form: FormBuilder, private nav: NavController, private loginService:LoginService) {
    this.loginForm = form.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
      rememberMe: ['', Validators.required]
    });
  }

  registration() {
    this.nav.setRoot(RegisterPage);
  }

  login(formData):void {
    this.loading = true;
    this.loginService.login(formData.username, formData.password, formData.rememberMe).subscribe(
      data => {
          this.loading = false;
          this.nav.setRoot(TabsPage);
      },
      err => {
        this.loading = false;
        this.errors = err.json().errors;
        console.log(this.errors);
      },
      () => console.log('login')
    );
  }


}