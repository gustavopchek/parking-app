import {Component} from '@angular/core';
import {NavController, NavParams, AlertController, Events} from 'ionic-angular';
import {PenaltyService} from '../../providers/penalty';
import {CustomerService} from '../../providers/customer';

@Component({
  templateUrl: 'build/pages/penalty/penalty.html',
  providers: [PenaltyService]
})
export class PenaltyPage {

  penalty;
  loading = false;
  errors = [];

  constructor(private navCtrl: NavController, private params: NavParams, private alertCtrl: AlertController, private penalties: PenaltyService, private customerService: CustomerService, public events: Events) {
    this.penalty = params.get('penalty');
    console.log(this.penalty);
  }

  payPenalty(id) {

    var result = '';

    var confirm = this.alertCtrl.create({
      title : 'Pagamento de Multa',
      message : 'Confirmar pagamento da multa?',
      buttons: [
        {text: 'Cancelar', role:'cancel', handler : () => {}},
        {text: 'Sim', handler : () => {
          
          this.loading = true;
          this.penalties.payPenalty(id).subscribe(
            data => {
              this.errors = [];
              this.loading = false;
              this.penalty = data;
              this.penalty.created_at = new Date(this.penalty.created_at).toLocaleString([], {day: '2-digit', month: '2-digit', year: '2-digit', hour: '2-digit', minute:'2-digit'});
              this.customerService.getCustomer().subscribe();
              this.events.publish('penalties:updated', data);
              console.log('payment ok');
              let alert = this.alertCtrl.create({  title : 'Aviso', message : "Multa paga com sucesso.", buttons: ['OK']});
              alert.present();
            },
            err => {
              this.loading = false;
              this.errors = err.json().errors;
                console.error(err)
              let alert = this.alertCtrl.create({  title : 'Aviso', message : "Erro ao pagar multa.", buttons: ['OK']});
            },
            () => null
          );

          confirm.dismiss().then(() => {

          })
        }}
      ]})
      
    confirm.present();

  }

}
