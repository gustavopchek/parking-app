import {Component} from '@angular/core';
import {HomePage} from '../home/home';
import {ActivationsPage} from '../activations/activations';
import {PenaltiesPage} from '../penalties/penalties';
import {PurchasesPage} from '../purchases/purchases';

@Component({
  templateUrl: 'build/pages/tabs/tabs.html'
})
export class TabsPage {

  private tab1Root: any;
  private tab2Root: any;
  private tab3Root: any;
  private tab4Root: any;

  constructor() {
    this.tab1Root = HomePage;
    this.tab2Root = ActivationsPage;
    this.tab3Root = PenaltiesPage;
    this.tab4Root = PurchasesPage;
  }
}
