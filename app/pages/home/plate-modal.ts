import {Component, provide} from '@angular/core';
import {NavController, ViewController, AlertController} from 'ionic-angular';
import {Plate} from '../../providers/plate';
import {Http,RequestOptions, XHRBackend} from '@angular/http';
import {AuthorizationHandler} from '../../utils/authorization-handler';
// import { TextMaskModule } from 'angular2-text-mask';

@Component({
  templateUrl: 'build/pages/home/plate-modal.html',
  providers: [Plate,
    provide(Http,{
      useFactory:(xhrBackend: XHRBackend, requestOptions: RequestOptions) => {
        return new AuthorizationHandler(xhrBackend, requestOptions);
      },
      deps: [XHRBackend, RequestOptions],
      multi:false
    })
  ]
})
export class PlateModal {

  plateParams = {code: ''};
  errors = [];
  loading;
  customerPlates;
  public mask = ['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];

  constructor(private viewCtrl: ViewController, private plate: Plate, private alertCtrl: AlertController) {
  }

  ngOnInit() {
    this.getCustomerPlates();
  }

  getCustomerPlates() {
    this.plate.customerPlates().subscribe(
      data => {
        this.customerPlates = data.json();
        // for (var key in this.foundActivations) {
        //   this.foundActivations[key].created_at = new Date(this.foundActivations[key].created_at).toLocaleString([], {day: '2-digit', month: '2-digit', year: '2-digit', hour: '2-digit', minute:'2-digit'});
        // };
      },
      err => console.error(err),
      () => console.log('getCustomerPlates completed')
    );
  }

  addPlate() {
    this.loading = true;
    // console.log(params);
    var params = JSON.stringify({plate : 
    	this.plateParams
    });
    this.plate.addPlate(params).subscribe(
      data => {
        console.log('plate added');
        this.loading = false;
        // let alert = this.alertCtrl.create({  title : 'Aviso', message : "Placa cadastrada", buttons: ['OK']});
        // alert.present();
        this.viewCtrl.dismiss(data);
      },
      err => {
        this.loading = false;
        this.errors = [];
          var errors = err.json().errors;
          for (var field in errors) {  
            for(var item of errors[field]){
              this.errors.push(item);
            }
          };
          console.log(this.errors);
      },
      () => console.log('plate added ok')
    );
  }

  deleteWithConfirmation(){
    
  }

  removePlate(id) {

    var result = '';

    var confirm = this.alertCtrl.create({
      title : 'Excluir placa',
      message : 'Confirmar exclusão?',
      buttons: [
        {text: 'Cancelar', role:'cancel', handler : () => {}},
        {text: 'Sim', handler : () => {
          
          this.loading = true;
          this.plate.removePlate(id).subscribe(
            data => {
              this.errors = [];
              this.loading = false;
              result = "Placa excluída.";
              this.getCustomerPlates();
              let alert = this.alertCtrl.create({  title : 'Aviso', message : result, buttons: ['OK']});
            },
            err => {
              this.loading = false;
              result = "Não foi possível excluir a placa."
              console.error(err);
              let alert = this.alertCtrl.create({  title : 'Aviso', message : result, buttons: ['OK']});
            },
            () => null
          );

          confirm.dismiss().then(() => {

          })
        }}
      ]})
      
    confirm.present();

  }


  dismiss(data) {
    this.viewCtrl.dismiss(data);
  }

}