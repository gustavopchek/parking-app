import {Component} from '@angular/core';
import {NavController, ModalController, ViewController, MenuController, NavParams, Events} from 'ionic-angular';
import {Ticket} from '../../providers/ticket/ticket';
import {Activation} from '../../providers/ticket/activation';
import {CustomerService} from '../../providers/customer';
import {Customer} from '../../models/customer';
import {Plate} from '../../providers/plate';
import {PlateModal} from './plate-modal';
import {StorageUtils} from '../../utils/storage.utils';
import {StorageService} from '../../providers/storage';
import {CashPage} from '../cash/cash';

@Component({
  templateUrl: 'build/pages/home/home.html',
  providers: [Ticket, Activation, Plate, CustomerService]
})
export class HomePage {

	foundTickets;
	active;
	activation;
	connected;
  loading;
  errors = [];
  customerPlates;
  activationParams = {ticket_id: '', plate_id:''};
  customer: Customer;
  cash;
	// public lastActivation;

  constructor(private navCtrl: NavController, private menuCtrl: MenuController, private modalCtrl: ModalController, private tickets: Ticket, private activations: Activation, private plate: Plate, private customerService: CustomerService, public events: Events) {

    this.events.subscribe('customer:updated', (customerData) => {
      this.customer = customerData[0];
    });
  }

  ngOnInit() {
    this.getCustomerPlates();
    this.getTickets();
    this.active = false;
    this.connected = true;
    this.loading = false;
    this.check();
    setInterval(() => this.check(), 30000);
    this.menuCtrl.enable(true);
    this.customer = StorageUtils.getAccount();
  }

  openCash(){
    this.navCtrl.push(CashPage);
  }


  check(){
  	this.activations.lastActivation().subscribe(
      data => {
          if(this.connected == false){
            this.getTickets();
            this.getCustomerPlates();
            this.connected = true;
          }
          this.activation = data.json();
          console.log(this.activation);
          if(this.activation.remaining > 0){
          	this.active = true;
          }else{
          	this.active = false;
          }
      },
      err => {
      	console.error(err);
      	this.connected = false;
      },
      () => console.log('checked')
    );
  }

  getTickets() {
    this.tickets.getTickets().subscribe(
      data => {
          this.foundTickets = data.json();
      },
      err => console.error(err),
      () => console.log('getTickets completed')
    );
	}



  getCustomerPlates() {
    this.plate.customerPlates().subscribe(
      data => {
        this.customerPlates = data.json();
      },
      err => console.error(err),
      () => console.log('getCustomerPlates completed')
    );
  }

  submit() {
    this.loading = true;
    // console.log(params);
    var params = JSON.stringify({activation : 
      this.activationParams
    });
    this.activations.createActivation(params).subscribe(
      data => {
        console.log('created');
        this.activation = data;
        this.active = true;
        this.loading = false;
        console.log(this.activation);
        this.errors = [];
        this.customerService.getCustomer().subscribe();
        this.events.publish('activations:updated', this.activation);
      },
      err => {
        this.loading = false;
        this.check();
        this.errors = err.json().errors;
        console.log(this.errors);
      },
      () => console.log('activation created')
    );
  }

  setPlate() {
    let plateModal = this.modalCtrl.create(PlateModal);
    plateModal.present();
    plateModal.onDidDismiss((data: {  }) => {
      this.getCustomerPlates();
    });
  }

}

// @Component({
//   templateUrl: 'build/pages/home/plate-modal.html',
// })
// class Plate {

//  constructor(public viewCtrl: ViewController) {

//  }

//  dismiss() {
//    let data = { 'foo': 'bar' };
//    this.viewCtrl.dismiss(data);
//  }

// }
