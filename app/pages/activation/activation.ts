import {Component} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import {Activation} from '../../providers/ticket/activation.ts';

@Component({
  templateUrl: 'build/pages/activation/activation.html',
  providers: [Activation]
})
export class ActivationPage {

  activation: Activation;

  constructor(private navCtrl: NavController, private params: NavParams) {
    this.activation = params.get('activation');
  }
	ngOnInit() {
    // this.getActivations();
  }
}
