

import {Page, NavController, Alert, Events} from 'ionic-angular';
import {Inject, Component} from '@angular/core';
import {FormBuilder, Validators, FormGroup, AbstractControl} from '@angular/forms';
import {Customer,Profile} from '../../models/customer';
// import {TranslatePipe, TranslateService} from 'ng2-translate/ng2-translate';
import {RegisterService} from '../../providers/register';
import {CustomerService} from '../../providers/customer';
import {TabsPage} from '../tabs/tabs';
import {Response} from '@angular/http';
import {StorageUtils} from '../../utils/storage.utils';
import {LoginPage} from '../login/login';

@Component({
    selector:'register-page',
    templateUrl: 'build/pages/register/register.html',
    providers:[RegisterService]
})
export class RegisterPage {

    registerForm: FormGroup;
    username: AbstractControl;
    password: AbstractControl;
    errors = [];
    loading = false;
 
    constructor(private nav: NavController, private fb: FormBuilder, private registerService:RegisterService, public customerService: CustomerService) {
        this.registerForm = fb.group({  
            name: ['', Validators.compose([Validators.required])],
            phone: ['', Validators.compose([Validators.required])],
            cpf: ['', Validators.compose([Validators.required])],
            email: ['', Validators.compose([Validators.required, Validators.minLength(4)])],
            password: ['', Validators.compose([Validators.required, Validators.minLength(6)])],
            passwordConfirmation: ['', Validators.compose([Validators.required, Validators.minLength(6)])]
        });
  
        this.username = this.registerForm.controls['username'];     
        this.password = this.registerForm.controls['password'];  
    }

    loginPage() {
        // this.nav.push(LoginPage);
        this.nav.setRoot(LoginPage);
    }


    register(formData):void {
      // This will be called when the user clicks on the Login button
      this.loading = true;
      console.log(formData);
      // console.log(password);
      this.registerService.register(formData.name, formData.phone, formData.cpf, formData.email, formData.password, formData.passwordConfirmation).subscribe(
        data => {
            this.loading = false;
            // this.events.publish('customer:updated', customer);
            this.customerService.getCustomer().subscribe();
            this.nav.setRoot(TabsPage);
        },
        err => {
          // console.log(err.json());
          this.loading = false;
          this.errors = [];
          var errors = err.json().errors;
           // err.json().errors.forEach((errors, field) => {
          //   // this.fb.valid = false;
          //   this.errors[field] = errors.join(', ');
          // });
          // console.log(err.json().errors);
          for (var field in errors) {
          //   // this.fb.valid = false;
            for(var item of errors[field]){
              this.errors.push(item);
              // this.registerForm.email
            }
            // this.errors[field] = errors[field];
          //   console.log(field);
          //   console.log(errors);
          };
          // this.errors.push("aaa");
          // this.errors.push("aaa");
          console.log(this.errors);
        },
      () => console.log('login')
      );
    }

    // registera(formData):void {
    //     // This will be called when the user clicks on the Register button
    //     event.preventDefault();
    //     this.loading = true;
    //     this.registerService.register(name, phone, cpf, email,password,passwordRepeat).subscribe(
    //         data => {
    //             this.loading = false;
    //             // this.nav.s7etRoot(TabsPage);
    //         },
    //         err => {
    //           this.loading = false;
    //           console.error(err)
    //         },
    //         () => console.log('register')
    //     );
    //     // this.registerService.register(username,password,rememberMe).subscribe(() => this.nav.setRoot(TabsPage));
    //     // this.nav.setRoot(TabsPage);
    // }
}