export enum Profile {
    ADMIN,
    USER,
    MANAGER
}

export class Customer {
    id:number;
    name:string;
    phone:string;
    cpf:string;
    email:string;
    password:string;
    cash:number;
    token: string;
    rememberMe:boolean;
    constructor(customer?:{id: number, name: string, email: string, phone: string, cash: number, token: string}) {
        // this.token = token;
        this.rememberMe = false;
        console.log(customer);
        if(customer) {
            this.id = customer.id;
            this.name = customer.name;
            this.phone = customer.phone;
            this.email = customer.email;
            this.cash = customer.cash;
            // _.assignIn(this, customer);
        }
    }
}